package org.mswsplex.basic.utils;

public class Utils {
	public static String worldTime(long time) {		
		long gameTime = time, hours = gameTime / 1000 + 6, minutes = (gameTime % 1000) * 60 / 1000; String ampm = "AM";
		if (hours >= 12) { hours -= 12; ampm = "PM"; } if (hours >= 12) { hours -= 12; ampm = "AM"; } if (hours == 0) hours = 12;
		String mm = "0" + minutes; mm = mm.substring(mm.length() - 2, mm.length());
		return hours + ":" + mm + " " + ampm;
		}
}
